import { Reservation } from '@/entities';
import { fetchAllReservation, findByReservationId } from '@/reservation-service';
import CardReservation from '@/src/components/CardReservation'
import Header from '@/src/components/Layouts/Header'
import Navbar from '@/src/components/Layouts/Navbar'
import { GetServerSideProps } from 'next';
import React from 'react'


interface Props {
  reservations: Reservation[];
  reservationById: Reservation[][]
}



function reservation({ reservations, reservationById }: Props) {


  return (
<>
    <div className='shadow-xl '>
        <Navbar />
        <Header />
      </div>

      {/* <div className='flex flex-row space-x-10 justify-center w-full overflow-x-scroll mt-20'>
      <CardReservation reservations={reservations}/>
      <CardReservation reservations={reservations}/>
      <CardReservation reservations={reservations}/>
      <CardReservation reservations={reservations}/>
      </div> */}

<div className="mt-20">

<div id='NosMenus' className='h-full overflow-x-auto w-screen '>
  <h3 className='text-bold text-white text-2xl text-center pt-10'>Toutes les réservations</h3>
  <div className="flex flex-row justify-center space-x-10 p-5 ">
    <div className='flex flex-row'>
      <div className='flex flex-row space-x-10'>
        {reservations.map(item =>
          <CardReservation reservations={item} key={item.id} />
          
        )}
      </div>
    </div>
  </div>
</div>
</div>

</>

  )
}

export default reservation



export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      reservations: await fetchAllReservation(),
      reservationById: await findByReservationId()
    }
  }
}