import { CartContextProvider } from '@/Cart-Context'
import '@/styles/globals.css'
import { Session } from 'inspector'
import { NextPage } from 'next'
import type { AppProps } from 'next/app'
import { ReactElement, ReactNode } from 'react'

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
  pageProps: { auth?: boolean; session?: Session }
}



export default function App({ Component, pageProps }: AppPropsWithLayout) {
  // Use the layout defined at the page level, if available
  const getLayout = Component.getLayout ?? ((page) => page)
  return <CartContextProvider>
  {getLayout(<Component {...pageProps} />)}
  </CartContextProvider>
}

