import Cart from "@/src/components/Cart";
import Header from "@/src/components/Layouts/Header";
import Navbar from "@/src/components/Layouts/Navbar";
import { } from 'flowbite'
// import { validateHeaderValue } from "http";

export default function reservez() {
  return (
    <>
      <div className='shadow-xl '>
        <Navbar />
        <Header />
      </div>

      <div className="flex flex-col space-y-20">

        <div className="text-center lg:text-left space-y-10 lg:mx-40">
          <h2 className='text-bold text-[#164C73] text-2xl text-center pt-10'>
            Reservation
          </h2>

          {/* <div className="flex flex-row justify-center space-y-10 space-x-10 py-2"></div>  */}
          <input type="text" id="helper-text" aria-describedby="helper-text-explanation" className=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Nom" />
          <input type="text" id="helper-text" aria-describedby="helper-text-explanation" className=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Prenom" />
          <input type="number" id="helper-text" aria-describedby="helper-text-explanation" className=" bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Téléphone" />
        </div>


        <div className=" text-center lg:text-left space-y-10 lg:mx-40 mb-10">
          <h3 className='text-bold text-[#164C73] text-2xl text-center pt-10'>
            Votre commande
          </h3>
          <div>
            <Cart/>
          </div>
        </div>

        <div className=" text-center lg:text-left space-y-10 lg:mx-40 mb-10">
          <h3 className='text-bold text-[#164C73] text-2xl text-center pt-10'>
            Pour combien de personnes ?
          </h3>
          <div className="flex justify-center space-x-20 ">
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">1</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">2</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">3</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">4</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">5</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">6</button>
          </div>
        </div>

        <div className=" text-center lg:text-left space-y-10 lg:mx-40 mb-5">
          <h3 className='text-bold text-[#164C73] text-2xl text-center pt-10'>
            Pour quelle date ?
          </h3>
          <div className="relative max-w-sm">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3.5 pointer-events-none">
              <svg className="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20">
                <path d="M20 4a2 2 0 0 0-2-2h-2V1a1 1 0 0 0-2 0v1h-3V1a1 1 0 0 0-2 0v1H6V1a1 1 0 0 0-2 0v1H2a2 2 0 0 0-2 2v2h20V4ZM0 18a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V8H0v10Zm5-8h10a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2Z" />
              </svg>
            </div>
            <input datepicker="true" type="text" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date" />
          </div>
        </div>


        <div className=" text-center lg:text-left space-y-10 lg:mx-40 mb-10">
          <h3 className='text-bold text-[#164C73] text-2xl text-center pt-10'>
            Pour quelle heure ?
          </h3>
          <div className="flex justify-center space-x-20 ">
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">12</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">13</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">14</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">15</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">16</button>
          </div>
          <div className="flex justify-center space-x-20 ">
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">17</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">18</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">19</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">20</button>
            <button className="bg-[#BFCCD9] hover:bg-[#164C73]  text-white font-bold py-2 px-4 shadow-xl  rounded">21</button>
          </div>
        </div>

        <div className="flex flex-row justify-center space-x-10 p-5 ">
          <button className="bg-[#164C73] hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-800 hover:border-blue-500 rounded">
            Valider !
          </button>

          <button className="bg-[#164C73] hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-800 hover:border-blue-500 rounded">
            Annuler !
          </button>
        </div>


      </div>
    </>

  )
}