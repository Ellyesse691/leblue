import Accueil from '@/src/components/Accueil'
import Header from '@/src/components/Layouts/Header'
import Navbar from '@/src/components/Layouts/Navbar'
import React from 'react'
import { Menuitem, Restaurant, } from '@/entities'
import { fetchRestaurant } from '@/restaurant-service';
import { GetServerSideProps } from 'next';
import Contact from '@/src/components/Contact';
import { fetchMenuitem } from '@/menuitem-service'
import Card from '@/src/components/Card'
import ReservationsComponent from '@/src/components/ReservationsComponent'



interface Props {
  restaurant: Restaurant;
  menuitem: Menuitem[];
}



function index({ restaurant, menuitem }: Props) {
  return (
    <>
      <div className='shadow-xl '>
        <Navbar />
        <Header />
      </div>


      <div className='mt-20' >
        <Accueil restaurant={restaurant} />
      </div>

      <div className="mt-20 bg-[#336385]">

        <div id='NosMenus' className=' w-full overflow-y-auto h-screen'>
          <h3 className='text-bold text-white text-2xl text-center pt-10'>Nos Menus</h3>
          <div className="flex flex-row justify-center space-x-10 p-5 ">
            <div className='flex flex-col'>
              <div className='flex flex-col space-y-10'>
                {menuitem.map(item =>
                  <Card menuitem={item} key={item.id} />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='mt-20'>
        <ReservationsComponent />
      </div>

      <div className='mt-20'>
        <Contact restaurant={restaurant} />
      </div>

    </>
  )
}

export default index

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      restaurant: await fetchRestaurant(1),
      menuitem: await fetchMenuitem(),
    }
  }
}
