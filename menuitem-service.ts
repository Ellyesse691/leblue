import axios from "axios";
import { Menuitem } from "./entities";


export async function fetchMenuitem() {
    const response = await axios.get<Menuitem[]>('http://localhost:8000/api/menuitem');
    return response.data

};