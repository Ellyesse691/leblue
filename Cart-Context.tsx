import { useEffect, useState } from "react";
import { createContext } from "react";
import { Menuitem } from "./entities";
import { setCookie, parseCookies } from "nookies";



interface CartState {
    menuitems:Menuitem[],
    addProduct : (value: Menuitem) => void,
    removeProduct : (value: Menuitem) => void,
    clear: () => void
    
}

export const CartContext = createContext({} as CartState);


export const CartContextProvider = ({children}:any) => {
    const [menuitems, setMenuitems] = useState<Menuitem[]>([]);

    useEffect (() => {
        const { cart } = parseCookies(null, "cart");
        if (cart) {
            setMenuitems(JSON.parse(cart));
        }
    },[])


    function addProduct(value:Menuitem) {
        setMenuitems([...menuitems, value])
        setCookie(null, 'cart', JSON.stringify(menuitems)) 
    }

    function removeProduct(value:Menuitem) {
        setMenuitems(menuitems.filter((item) => item.id !== value.id));
        setCookie(null, 'cart', JSON.stringify(menuitems))
    }

    function clear() {
        setMenuitems([])
        setCookie(null, 'cart', JSON.stringify(menuitems)) 
    }

    return (
        <CartContext.Provider value={{menuitems, addProduct, removeProduct, clear}}>
            {children}
        </CartContext.Provider>
    )

}