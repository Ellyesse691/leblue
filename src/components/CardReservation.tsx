import { Reservation } from '@/entities';
import React from 'react'




interface Props {
    reservations: Reservation;
  }

function CardReservation({ reservations }: Props) {
  return (
    // <div className='flex justify-center w-full overflow-x-auto mt-20 '>
      <div className="max-w-sm rounded overflow-hidden shadow-lg">

  <div className="px-6 py-4">
    <div className="font-bold text-xl mb-2">{reservations.nom}</div>
    <p className="text-gray-700 text-base">
    {reservations.prenom}    
    </p>
  </div>

  <div className="px-6 pt-4 pb-2">
    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{reservations.menuitem}</span>
    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{reservations.nom}</span>
    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{reservations.nom}</span>
  </div>
</div>
    // </div>
  )
}

export default CardReservation
