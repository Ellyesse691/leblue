import { CartContext } from "@/Cart-Context"
import { useContext, useState } from "react"
import { parseCookies } from "nookies";
import { Menuitem } from "@/entities";




export default function Cart() {

    const { menuitems, removeProduct, clear } = useContext(CartContext);

    // const { token, setToken } = useContext(AuthContext);

    // const [order, setOrder] = useState<Order>({
    //     product: products,
    //     deliveryMode: "A domicile"
    // })

    function totalCount() {
        let total = 0;
        menuitems.forEach((item: Menuitem) =>
            total += Number(item.prix)
        );
        return total
    }

    function confirmCart() {
        clear();
    }



    return (

        <div className="flex flex-col justify-center">
            <div className="flex  flex-row justify-center mt-5">


                {totalCount() > 0 &&
                    <div className="flex flex-col mb-3">

                        <ul className="list-group">

                            {menuitems?.map((item: Menuitem) =>

                                <li className="list-group-item" key={item.id}>
                                    <div className="flex flex-row">

                                        <div className="flex flex-col">
                                            <div key={item.id} className="flex flex-col items-center bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700">
                                                <img className="object-cover w-full rounded-t-lg h-96 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg" src={item.image} alt="" />
                                                <div className="flex flex-col justify-between p-4 leading-normal">
                                                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{item.titre}</h5>
                                                    <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">{item.description}</p>
                                                    <p>{item.prix}</p>
                                                </div>
                        

                                                <button className="text-3xl" onClick={() => removeProduct(item)}>-</button>
                                           
                                            </div>

                                        </div>

                                    </div>
                                </li>

                            )}
                            <li className="text-center">
                                <p>SOUS-TOTAL : {totalCount()} €</p>
                            </li>
                        </ul>
                    </div>
                }






            </div>
        </div>


    )
}