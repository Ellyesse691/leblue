import {ChevronDownIcon} from '@heroicons/react/24/solid'
import Link from 'next/link'
import { useRouter } from "next/router"
import React, { useState } from "react"
import ReactCountryFlag from "react-country-flag"

export default function LanguageSelectDropdown ({setOpenDrawer=null}: any){
    const router = useRouter()
    const {locale} = router
    const currentLocale = locale === 'en' ? 'EN':'FR'
    const otherLocale = locale === 'en' ? 'FR':'EN'

    const [open, setOpen] =useState(false)

    return(
        <div className="relative">
<div
onClick={()=>setOpen(true)}
className="flex items-center space-x-2 p-1 rounded-xl cursor-pointer hover:underline">
    <ReactCountryFlag
    countryCode={currentLocale === "EN"?"US": currentLocale}
    svg
    style={{
        width: '1.5em',
        height: '1.5em',
    }}
    title={currentLocale}
    className='rounded-full'
    />
    <span className='text-sm text-bold text-[#8D604B]'>{currentLocale}</span>
    <ChevronDownIcon className='w-5'/>
</div>
{open &&
<>
<div
onClick={()=>setOpen(false)}
className='fixed top-0 left-0 w-screen h-screen'>
</div>

<Link href={router.asPath} locale={otherLocale.toLocaleLowerCase()} scroll={false}>
<div
onClick={()=>{
    setOpen(false)
    if (setOpenDrawer != null) setOpenDrawer (false)
}}
className='absolute -bottom-10 z-10 flex items-center space-x-2 p-1 border border-gray-500/40 rounded-xl cursor-pointer bg-pastisse-white hover:bg-pastisse-beige'>
    <ReactCountryFlag
    countryCode={otherLocale === "EN" ? "US" : otherLocale}
    svg
    style={{
        width: '1.5em',
        height: '1.5em',
        borderRadius: '1em'
    }}
    title={otherLocale}
    className='rounded-full'
    />
    <span className='text-sm text-[#8D604B]'>{otherLocale} </span>
    <ChevronDownIcon className='w-5 opacity-0'/>
</div>
</Link>
</>}

        </div>
    )
}