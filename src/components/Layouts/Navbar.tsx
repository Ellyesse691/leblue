import React, { useState } from 'react'
import LanguagesSelectDropdown from './LanguagesSelectDropdown'
import { useRouter } from 'next/router'
import {Bars3Icon, ChevronDownIcon} from '@heroicons/react/24/solid'



function Navbar() {
  
    const [openDrawer, setOpenDrawer] = useState(false)

    const router = useRouter()
    const { locale } = router
    return (
      <>
        <div>
          <div className='flex w-full items-center justify-end bg-[#EFF1F2] h-20'>
            {/* <a href="/">
                <Image
                  src={"/img/logo.png"}
                  alt={"Next.js Logo"}
                  width={50}
                  height={50}
                  className='flex justify-start -left-0 absolute pl-2'
                  />
                  </a> */}
   
            <nav className='hidden lg:flex'>
              <ul className='flex  items-center text-[#164C73] font-semibold text-lg'>
                <li className='nav-link p-5'>
                  <a href="/">{locale === 'fr' ? 'Home' : 'Accueil'}
                  </a>
                </li>
                <li className='nav-link p-5'>
                  <a href="/#NosMenus">{locale === 'fr' ? 'Our Menus' : 'Nos Menus'}</a>
                </li>
                <li className='nav-link p-5'>
                  <a href="/#Reservation">{locale === 'fr' ? 'Reservation' : 'Reservation'}</a>
                </li>
                <li className='nav-link p-5'>
                  <a href="/#Contact">{locale === 'fr' ? 'Contact' : 'Contact'}</a>
                </li>
              </ul>
            </nav>
            <Bars3Icon
              onClick={() => setOpenDrawer(true)}
              className='w-7 text-blue-500 cursor-pointer lg:hidden inline pt-1 '
            />
  
            <LanguagesSelectDropdown />
            {openDrawer &&
  
              <>
                <div
                  onClick={() => setOpenDrawer(false)}
                  className='fixed top-0 left-0 w-screen h-screen bg-[#222222]/20 '>
                </div>
                <LanguagesSelectDropdown />
              </>
            }
          </div>
          <div
            className={`fixed flex justify-center z-50 w-[275px] h-screen left-0 top-0 bg-[#6C4A3B] 
                ease-in-out duration-500 ${openDrawer ? 'translate-x-0 && border-r border-orange-600/50' : '-translate-x-full'}`}>
            <nav className='flex items-center justify-center text-[#8D604B] text-lg absolute top-56'>
              <ul className='p-5'>
                <li
                  key={1}
                  onClick={() => setOpenDrawer(false)}
                  className='nav-link p-3'>
                  <a href="/">
                    {locale === 'fr' ? 'Accueil' : 'Home'}
                  </a>
                </li>
                <li
                  key={2}
                  onClick={() => setOpenDrawer(false)}
                  className='nav-link p-3'>
                  <a href="/#NosMenus">{locale === 'fr' ? 'Menus' : 'Menus'}</a>
                </li>
                <li
                  key={3}
                  onClick={() => setOpenDrawer(false)}
                  className='nav-link p-3'>
                  <a href="/#Reservation">{locale === 'fr' ? 'Reservation' : 'Reservation'}</a>
                </li>
                <li
                  key={4}
                  onClick={() => setOpenDrawer(false)}
                  className='nav-link p-3'>
                  <a href="/#Contact">{locale === 'fr' ? 'Contact' : 'Contact'}</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </>























    
    // <div className='flex w-full bg-[#EFF1F2] items-center justify-end h-20'>
    //    <nav className='hidden lg:flex'>
    //         <ul className='flex  items-center text-[#164C73] font-semibold text-lg'>
    //           <li className='nav-link p-5'>
    //             <a href="#Accueil">Accueil</a>
    //           </li>
    //           <li className='nav-link p-5'>
    //             <a href="#NosMenus">NosMenus</a>
    //           </li>
    //           <li className='nav-link p-5'>
    //             <a href="#Reservation">Reservation</a>
    //           </li>
    //           <li className='nav-link p-5'>
    //             <a href="#Contact">Contact</a>
    //           </li>
    //         </ul>
    //       </nav>
    // </div>
  )
}

export default Navbar
