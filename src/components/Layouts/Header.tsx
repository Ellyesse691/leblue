import React from 'react'

function Header() {
  return (
    <div className='flex w-full bg-[#EFF1F2] items-center justify-center h-20 p-5'>
     <a href="/">
       <h1 className='text-[#164C73] text-4xl'>LE BLUE</h1>
      </a>
    </div>
  )
}

export default Header
