import React from 'react'

function ReservationsComponent() {
  return (
    <div id='Reservation' className=' w-full h-full'>
      <h3 className='text-bold text-[#164C73] text-2xl text-center pt-10 mb-10'>Reservation</h3>
      <div className="text-center lg:text-left flex flex-col space-y-10 lg:mx-40">
        <p>Réservations : Les réservations peuvent être effectuées en ligne via notre site web ou par téléphone au moins 24 heures à l'avance. Les réservations de dernière minute sont acceptées en fonction de la disponibilité.</p>

        <p>Taille du Groupe : Nous accueillons des groupes de toutes tailles, des dîners en tête-à-tête aux rassemblements plus importants. Veuillez spécifier le nombre exact de personnes lors de la réservation pour que nous puissions préparer au mieux votre table.</p>

        <p>Heures de Service : Nous offrons des services pour le déjeuner et le dîner. Les heures d'ouverture et de fermeture sont disponibles sur notre site web. Veuillez arriver à l'heure pour que nous puissions garantir une expérience agréable pour tous nos clients.</p>

        <p>Carte de Crédit : Une carte de crédit valide est requise pour garantir toutes les réservations. Aucun frais ne sera facturé au moment de la réservation, mais en cas d'annulation tardive (voir ci-dessous), des frais pourraient s'appliquer.</p>

        <p> Annulations : Si vous devez annuler ou modifier votre réservation, veuillez le faire au moins 6 heures avant l'heure de réservation prévue. Les annulations tardives pourraient entraîner des frais correspondant à une partie du montant de la réservation, en fonction du préavis donné.</p>

        <div className="flex flex-row justify-center space-x-10 p-5 ">
          <a className="bg-[#164C73] hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-800 hover:border-blue-500 rounded"
            href='/reservez'>
            Reservez !
          </a>
        </div>

        <p>Allergies et Restrictions Alimentaires : Nous sommes ravis de répondre à vos besoins alimentaires spécifiques, notamment les allergies et les restrictions alimentaires. Veuillez nous en informer au moment de la réservation afin que notre équipe puisse préparer des plats adaptés à vos préférences.</p>
        <p>Tenue Vestimentaire : Nous apprécions une tenue vestimentaire appropriée pour garantir une atmosphère agréable pour tous nos clients. Une tenue décontractée élégante est généralement recommandée.</p>
      <p>Nous nous engageons à offrir une expérience gastronomique mémorable et à prendre en compte vos besoins individuels. Si vous avez des demandes spéciales ou des questions concernant les conditions de réservation, n'hésitez pas à nous contacter. Au plaisir de vous accueillir bientôt au Blue.</p>
      </div>

      <div className="flex flex-row justify-center space-x-10 p-5 ">
          <a className="bg-[#164C73] hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-800 hover:border-blue-500 rounded"
            href='/reservation'>
            voir toutes les reservationes</a>
         
        </div>
    </div>


  )
}

export default ReservationsComponent
