import { Restaurant } from '@/entities';
import React from 'react'



interface Props{
    restaurant: Restaurant;
}

function Contact({restaurant}: Props) {
  return (
    <div id='Contact' className=' w-full h-full'>
    <h3 className='text-bold text-[#164C73] text-2xl text-center pt-10'>Nous Contacter</h3>
    <div className="flex flex-row justify-center space-x-10 p-5 ">
      <div className='flex flex-col'>
        <div className='flex flex-row space-x-10'>
        
         <p>{restaurant.portable}</p>
         
        </div>
        
      <p>{restaurant.mail}</p>

        </div>
      </div>     
    </div>

  )
}

export default Contact
