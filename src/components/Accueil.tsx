import { Restaurant } from '@/entities'
import React from 'react'

interface Props{
    restaurant: Restaurant;
}

function Accueil ({restaurant}: Props) {
  return (
    <div id='Accueil' className='h-full mt-10'>
      <div className='text-center lg:text-left flex flex-col space-y-10 lg:mx-40'>
        <img src={restaurant.image} alt=""></img>
        
        <p>Bienvenu.e.s "LeBlue" est un restaurant haut de gamme situé au cœur de la ville : {restaurant.adresse}, réputé pour offrir une expérience culinaire exceptionnelle alliant saveurs exquises, ambiance élégante et service attentif. Notre mission est de créer un lieu où les convives peuvent se détendre, socialiser et savourer une cuisine raffinée préparée avec passion et créativité  </p>
        <p>La décoration de "LeBlue" est un mariage subtil entre le moderne et le classique, créant ainsi une atmosphère chaleureuse et accueillante. Les tons de bleu et de blanc prédominent, évoquant une sensation de fraîcheur et d'apaisement, tout en reflétant également notre engagement envers des ingrédients frais et de qualité</p>
        <p>En ce qui concerne notre menu, nous mettons l'accent sur une cuisine gastronomique qui célèbre à la fois les saveurs locales et internationales. Notre équipe de chefs talentueux s'efforce de créer des plats qui marient tradition et innovation, en utilisant des ingrédients de saison pour garantir des plats à la fois délicieux et esthétiquement attrayants.</p>
        <p>Pour accompagner nos mets exquis, nous proposons une sélection soigneusement élaborée de vins provenant de différentes régions du monde, ainsi que des cocktails élégants préparés par nos mixologues experts.</p>
        <p>Le service à "LeBlue" est une priorité absolue. Notre personnel attentionné et professionnel est là pour vous guider à travers le menu, répondre à vos questions et s'assurer que votre expérience culinaire soit mémorable du début à la fin.</p>
        <p>Que ce soit pour une occasion spéciale, un dîner d'affaires ou simplement pour profiter d'une soirée exceptionnelle, "LeBlue" est l'endroit idéal. Nous sommes impatients de vous accueillir et de partager avec vous une expérience gastronomique inoubliable.</p>
        
      </div>
    </div>
  )
}

export default Accueil


