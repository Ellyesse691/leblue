
import { CartContext } from '@/Cart-Context';
import { Menuitem } from '@/entities';
import React, { useContext, useState } from 'react';

interface Props {
  menuitem:Menuitem;
}

export default function CartButton({menuitem}:Props) {

  const handleButtonClick = () => {
    addCart()
  };


  const {menuitems, addProduct} = useContext(CartContext);

  function addCart () {
    addProduct(menuitem)
    console.log(menuitems)
}



  return (
    <>

    
        <button className="text-3xl" onClick={handleButtonClick}>
           +
        </button>
    
    </>
  );
}
