

export interface Restaurant {
    id?: number;
    nom: string;
    capacite: number;
    adresse: string;
    mail: string;
    portable: number;
    image: string;
}

export interface Menuitem {
    map(arg0: (item: any) => import("react").JSX.Element): import("react").ReactNode;
    id?: number;
    titre: string;
    description: string;
    prix: number;
    categorie: string;
    image: string;
}

export interface Reservation {
    id? : number;
    nom: string;
    prenom: string;
    numero: number;
    nbpersonnes: number;
    date: string;
    heure: string;
    menuitem: [];
}