import axios from "axios";
import { Reservation } from "./entities";


export async function fetchAllReservation() {
    const response = await axios.get<Reservation[]>('http://localhost:8000/api/reservation');
    return response.data

};

export async function findByReservationId(id:number) {
    const response = await axios.get<Reservation[]>('http://localhost:8000/api/reservation/'+id);
    return response.data

};