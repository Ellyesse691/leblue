import axios from "axios";
import { Restaurant } from "./entities";


export async function fetchRestaurant(id:number) {
    const response = await axios.get<Restaurant>('http://localhost:8000/api/restaurant/' +id);
    return response.data

};